package cn.jesseyang.autosendsalaryemail.web;

import cn.jesseyang.autosendsalaryemail.email.MailUtil;
import cn.jesseyang.autosendsalaryemail.entity.Salary;
import cn.jesseyang.autosendsalaryemail.entity.SalaryVo;
import cn.jesseyang.autosendsalaryemail.entity.SingleSalaryVo;
import cn.jesseyang.autosendsalaryemail.excel.ExcelUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import sun.awt.ModalExclude;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class EmailController {
    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping("/excelfile")
    public ModelAndView excelFile(@RequestParam MultipartFile file){
        ModelAndView modelAndView = new ModelAndView("index");
        try {
            List<Salary> salaries = ExcelUtil.readExcel(file);
            modelAndView.addObject("salaryList",salaries);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @RequestMapping("/sendSalaryEmail")
    public String sendSalaryEmail(@RequestBody SalaryVo salaryVo){
        List<Salary> salaryList = salaryVo.getSalaryList();
        if(salaryList!=null && salaryList.size()>0){
            for (int i = 0; i < salaryList.size(); i++) {
                MailUtil.sendEmailBySalary(salaryVo.getSalaryList().get(i),salaryVo.getYear(),salaryVo.getMonth());
            }
        }
        return "index";
    }

    @RequestMapping("/sendSingle")
    public String sendSalaryEmail(@RequestBody SingleSalaryVo salaryVo){
        MailUtil.sendEmailBySalary(salaryVo.getSalary(),salaryVo.getYear(),salaryVo.getMonth());
        return "index";
    }
}
