package cn.jesseyang.autosendsalaryemail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoSendSalaryEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoSendSalaryEmailApplication.class, args);
    }

}
