package cn.jesseyang.autosendsalaryemail.email;


import cn.jesseyang.autosendsalaryemail.entity.Salary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.MimeMessage;


public class MailUtil {

    @Value("${jesse.account}")
    private static String emailAccount = "xiaodan@1510ad.com";
    @Value("${jesse.pwd}")
    private static String password = "Xd123456";

    private static String phone = "";

    private static JavaMailSenderImpl mailSender = createMailSender();

    private static JavaMailSenderImpl createMailSender() {
        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setHost("smtp.exmail.qq.com");
        sender.setPort(25);
        sender.setUsername(emailAccount);
        sender.setPassword(password);
        sender.setDefaultEncoding("Utf-8");
        Properties p = new Properties();
        p.setProperty("mail.smtp.timeout", "600000");
        p.setProperty("mail.smtp.auth", "false");
        sender.setJavaMailProperties(p);
        return sender;
    }

    public static void sendMail(String to, String html) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        // 设置utf-8或GBK编码，否则邮件会有乱码
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        messageHelper.setFrom(emailAccount, "肖丹");
        messageHelper.setTo(to);
        messageHelper.setSubject("test mail");
        messageHelper.setText(html, true);
        mailSender.send(mimeMessage);
    }


    public static boolean emailSend(String email) {
        String content = "<!DOCTYPE html>\n" +
                "<html lang='en'>\n" +
                "<head>\n" +
                "    <meta charset='UTF-8'>\n" +
                "    <meta name='viewport' content='width=device-width, initial-scale=1.0'>\n" +
                "    <meta http-equiv='X-UA-Compatible' content='ie=edge'>\n" +
                "    <title>Document</title>\n" +
                "    <style> \n" +
                "        \n" +
                "    </style>\n" +
                "</head>\n" +
                "<body >\n" +
                "    <div >\n" +
                "        <h1 style='text-align: center;height: 100px;line-height: 100px;'>这是一封测试邮件</h1>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</body>\n" +
                "</html>";
        try {
            sendMail(email, content);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static String sendEmailBySalary(Salary salary,String year,String month){
        String name = salary.getName();
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            // 设置utf-8或GBK编码，否则邮件会有乱码
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
            messageHelper.setFrom(emailAccount, "肖丹");
            messageHelper.setTo(salary.getEmail());
            messageHelper.setSubject(salary.getName()+year+"年"+month+"月工资条");
            messageHelper.setText(getHtmlContentBySalary(salary,year,month), true);
            mailSender.send(mimeMessage);
            return "1";
        } catch (MessagingException e) {
            e.printStackTrace();
            return name;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return name;
        }
    }

    private static String getHtmlContentBySalary(Salary salary, String year, String month) {
        DecimalFormat df = new DecimalFormat("0.00");
        String content = "<!DOCTYPE html>\n" +
                "<html lang='en'>\n" +
                "<head>\n" +
                "    <meta charset='UTF-8'>\n" +
                "    <meta name='viewport' content='width=device-width, initial-scale=1.0'>\n" +
                "    <meta http-equiv='X-UA-Compatible' content='ie=edge'>\n" +
                "    <title>Document</title>\n" +
                "    <style> \n" +
                "td{align:center}"+
                "        \n" +
                "    </style>\n" +
                "</head>\n" +
                "<body >\n" +
                "    <div >\n" +
                "        <h1 style='text-align: center;height: 100px;line-height: 100px;'>"+salary.getName()+year+"年"+month+"月工资条"+"</h1>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +

                "<table border=\"1\">\n" +
                "            <tr>\n" +
                "                <th>姓名</th>\n" +
                "                <th>部门</th>\n" +
                "                <th>标准出勤天数</th>\n" +
                "                <th>实际出勤天数</th>\n" +
                "                <th>事假</th>\n" +
                "                <th>病假</th>\n" +
                "                <th>年假</th>\n" +
                "                <th>调休</th>\n" +
                "                <th>婚假</th>\n" +
                "                <th>丧假</th>\n" +
                "                <th>陪产假</th>\n" +
                "                <th>产检</th>\n" +
                "                <th>产假</th>\n" +
                "                <th>旷工天数</th>\n" +
                "                <th>出差天数</th>\n" +
                "                <th>加班天数</th>\n" +
                "                <th>迟到次数</th>\n" +
                "                <th>迟到分钟</th>\n" +
                "                <th>早退次数</th>\n" +
                "                <th>早退分钟</th>\n" +
                "                <th>标准工资</th>\n" +
                "                <th>绩效/奖励</th>\n" +
                "                <th>提成</th>\n" +
                (salary.getHighSalary()==0?"":"                <th>高开</th>\n") +
                "                <th>餐补</th>\n" +
                "                <th>差旅补助</th>\n" +
                "                <th>通讯补助</th>\n" +
                "                <th>交通补助</th>\n" +
                "                <th>迟到</th>\n" +
                "                <th>事假</th>\n" +
                "                <th>扣款</th>\n" +
                "                <th>标准工资</th>\n" +
                "                <th>社保</th>\n" +
                "                <th>公积金</th>\n" +
                "                <th>税前工资</th>\n" +
                "                <th>个税</th>\n" +
                "                <th>税后工资</th>\n" +
                "            </tr>\n" +
                "<td align='center'  style=\"min-width:50px\" >"+salary.getName()+"</td>"+
                "<td align='center'  style=\"min-width:100px\">"+salary.getDepartment()+"</td>"+
                "<td align='center' >"+df.format(salary.getWorkday())+"</td>"+
                "<td align='center' >"+df.format(salary.getActualWorkday())+"</td>"+
                "<td align='center' >"+df.format(salary.getPersonalLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getIllnessLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getAnnualLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getAdjustLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getWeddingLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getFuneralLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getCompanyChildBirthLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getChildBirthCheckLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getChildBirthLeave())+"</td>"+
                "<td align='center' >"+df.format(salary.getNotWork())+"</td>"+
                "<td align='center' >"+df.format(salary.getBusinessTrip())+"</td>"+
                "<td align='center' >"+df.format(salary.getOverwork())+"</td>"+
                "<td align='center' >"+df.format(salary.getBeLateTimes())+"</td>"+
                "<td align='center' >"+df.format(salary.getBeLateMinutes())+"</td>"+
                "<td align='center' >"+df.format(salary.getEarlyTimes())+"</td>"+
                "<td align='center' >"+df.format(salary.getBeLateMinutes())+"</td>"+
                "<td align='center' >"+df.format(salary.getStandSalary())+"</td>"+
                "<td align='center' >"+df.format(salary.getPerformanceSalary())+"</td>"+
                "<td align='center' >"+df.format(salary.getExtraSalary())+"</td>"+
                (salary.getHighSalary()==0?"":"<td align='center' >"+df.format(salary.getHighSalary())+"</td>")
                +
                "<td align='center' >"+salary.getFood()+"</td>"+
                "<td align='center' >"+salary.getTravel()+"</td>"+
                "<td align='center' >"+salary.getPhone()+"</td>"+
                "<td align='center' >"+salary.getTransportation()+"</td>"+
                "<td align='center' >"+salary.getBeLateReduce()+"</td>"+
                "<td align='center' >"+salary.getPersonalLeaveReduce()+"</td>"+
                "<td align='center' >"+salary.getSalaryReduce()+"</td>"+
                "<td align='center' >"+salary.getIncome()+"</td>"+
                "<td align='center' >"+salary.getSocialSecurity()+"</td>"+
                "<td align='center' >"+salary.getAccumulationFund()+"</td>"+
                "<td align='center' >"+salary.getBeforeTax()+"</td>"+
                "<td align='center' >"+salary.getTax()+"</td>"+
                "<td align='center' >"+salary.getAfterTax()+"</td>"+
                "        </table>"+
                "<div>备注："+salary.getRemark()+"</div>"+
                "</body>\n" +
                "</html>";

        return content;
    }

}