package cn.jesseyang.autosendsalaryemail.excel;
import cn.jesseyang.autosendsalaryemail.entity.Salary;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItem;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Excel文档工具类
 */
public class ExcelUtil {


    public InputStream getInputStreamFromFileName(String fileName){
        //读取Excel文件
        File excelFile = new File(fileName.trim());
        try {
            return new FileInputStream(excelFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 从Excel文件读取数据
     * @param fileName 要读取的Excel文件的全路径文件名称
     * @return 从Excel文件中批量导入的用户数据
     */
    public static List<Salary> readExcel(String fileName) throws IOException {
        //读取Excel文件
        File excelFile = new File(fileName.trim());
        InputStream is = new FileInputStream(excelFile);

        Workbook workbook = null;
        Sheet sheet = null;
        Row row = null;

        //获取Excel工作薄
        if (excelFile.getName().endsWith("xlsx")) {
            workbook = new XSSFWorkbook(is);
        } else {
            workbook = new HSSFWorkbook(is);
        }
        if (workbook == null) {
            System.err.println("Excel文件有问题,请检查！");
            return null;
        }

        //获取Excel表单
        sheet = workbook.getSheetAt(0);

        List<Salary> salaries = new ArrayList<>();
        for(int rowNum = 2; rowNum <= sheet.getLastRowNum(); rowNum++) {
            //获取一行
            row = sheet.getRow(rowNum);
            Salary salary = new Salary();
            salary.setName(getStringCellValue(row.getCell(0)));
            salary.setDepartment(getStringCellValue(row.getCell(1)));
            salary.setWorkday(row.getCell(2).getNumericCellValue());
            salary.setActualWorkday(row.getCell(3).getNumericCellValue());
            salary.setPersonalLeave(row.getCell(4).getNumericCellValue());
            salary.setIllnessLeave(row.getCell(5).getNumericCellValue());
            salary.setAnnualLeave(row.getCell(6).getNumericCellValue());
            salary.setAdjustLeave(row.getCell(7).getNumericCellValue());
            salary.setWeddingLeave(row.getCell(8).getNumericCellValue());
            salary.setFuneralLeave(row.getCell(9).getNumericCellValue());
            salary.setCompanyChildBirthLeave(row.getCell(10).getNumericCellValue());
            salary.setChildBirthCheckLeave(row.getCell(11).getNumericCellValue());
            salary.setChildBirthLeave(row.getCell(12).getNumericCellValue());
            salary.setNotWork(row.getCell(14).getNumericCellValue());
            salary.setBusinessTrip(row.getCell(15).getNumericCellValue());
            salary.setOverwork(row.getCell(16).getNumericCellValue());
            salary.setSpecialOverwork(row.getCell(17).getNumericCellValue());
            salary.setBeLateTimes(row.getCell(18).getNumericCellValue());
            salary.setBeLateMinutes(row.getCell(19).getNumericCellValue());
            salary.setEarlyTimes(row.getCell(20).getNumericCellValue());
            salary.setEarlyMinutes(row.getCell(21).getNumericCellValue());
            salary.setStandSalary(row.getCell(24).getNumericCellValue());
            salary.setPerformanceSalary(row.getCell(25).getNumericCellValue());
            salary.setExtraSalary(row.getCell(26).getNumericCellValue());
            salary.setHighSalary(row.getCell(27).getNumericCellValue());
            salary.setFood(row.getCell(28).getNumericCellValue());
            salary.setTravel(row.getCell(29).getNumericCellValue());
            salary.setPhone(row.getCell(30).getNumericCellValue());
            salary.setTransportation(row.getCell(31).getNumericCellValue());
            salary.setBeLateReduce(row.getCell(32).getNumericCellValue());
            salary.setPersonalLeaveReduce(row.getCell(33).getNumericCellValue());
            salary.setSalaryReduce(row.getCell(34).getNumericCellValue());
            salary.setIncome(row.getCell(35).getNumericCellValue());
            salary.setSocialSecurity(row.getCell(36).getNumericCellValue());
            salary.setAccumulationFund(row.getCell(37).getNumericCellValue());
            salary.setBeforeTax(row.getCell(38).getNumericCellValue());
            salary.setTax(row.getCell(39).getNumericCellValue());
            salary.setAfterTax(row.getCell(40).getNumericCellValue());
            salary.setRemark(getStringCellValue(row.getCell(41)));
            salaries.add(salary);
        }
        is.close();
        return salaries;
    }

    public static List<Salary> readExcel(MultipartFile multipartFile) throws IOException {

        //读取Excel文件
        InputStream is = multipartFile.getInputStream();

        Workbook workbook = null;
        Sheet sheet = null;
        Row row = null;

        //获取Excel工作薄
        if (multipartFile.getOriginalFilename().endsWith("xlsx")) {
            workbook = new XSSFWorkbook(is);
        } else {
            workbook = new HSSFWorkbook(is);
        }
        if (workbook == null) {
            System.err.println("Excel文件有问题,请检查！");
            return null;
        }

        //获取Excel表单
        sheet = workbook.getSheetAt(0);

        List<Salary> salaries = new ArrayList<>();
        for(int rowNum = 2; rowNum <= sheet.getLastRowNum(); rowNum++) {
            //获取一行
            row = sheet.getRow(rowNum);
            if(null == getStringCellValue(row.getCell(0))){
                continue;
            }
            Salary salary = new Salary();
            salary.setName(getStringCellValue(row.getCell(0)));
            salary.setDepartment(getStringCellValue(row.getCell(1)));
            salary.setWorkday(row.getCell(2).getNumericCellValue());
            salary.setActualWorkday(row.getCell(3).getNumericCellValue());
            salary.setPersonalLeave(row.getCell(4).getNumericCellValue());
            salary.setIllnessLeave(row.getCell(5).getNumericCellValue());
            salary.setAnnualLeave(row.getCell(6).getNumericCellValue());
            salary.setAdjustLeave(row.getCell(7).getNumericCellValue());
            salary.setWeddingLeave(row.getCell(8).getNumericCellValue());
            salary.setFuneralLeave(row.getCell(9).getNumericCellValue());
            salary.setCompanyChildBirthLeave(row.getCell(10).getNumericCellValue());
            salary.setChildBirthCheckLeave(row.getCell(11).getNumericCellValue());
            salary.setChildBirthLeave(row.getCell(12).getNumericCellValue());
            salary.setNotWork(row.getCell(14).getNumericCellValue());
            salary.setBusinessTrip(row.getCell(15).getNumericCellValue());
            salary.setOverwork(row.getCell(16).getNumericCellValue());
            salary.setSpecialOverwork(row.getCell(17).getNumericCellValue());
            salary.setBeLateTimes(row.getCell(18).getNumericCellValue());
            salary.setBeLateMinutes(row.getCell(19).getNumericCellValue());
            salary.setEarlyTimes(row.getCell(20).getNumericCellValue());
            salary.setEarlyMinutes(row.getCell(21).getNumericCellValue());
            salary.setStandSalary(row.getCell(24).getNumericCellValue());
            salary.setPerformanceSalary(row.getCell(25).getNumericCellValue());
            salary.setExtraSalary(row.getCell(26).getNumericCellValue());
            salary.setHighSalary(row.getCell(27).getNumericCellValue());
            salary.setFood(row.getCell(28).getNumericCellValue());
            salary.setTravel(row.getCell(29).getNumericCellValue());
            salary.setPhone(row.getCell(30).getNumericCellValue());
            salary.setTransportation(row.getCell(31).getNumericCellValue());
            salary.setBeLateReduce(row.getCell(32).getNumericCellValue());
            salary.setPersonalLeaveReduce(row.getCell(33).getNumericCellValue());
            salary.setSalaryReduce(row.getCell(34).getNumericCellValue());
            salary.setIncome(row.getCell(35).getNumericCellValue());
            salary.setSocialSecurity(row.getCell(36).getNumericCellValue());
            salary.setAccumulationFund(row.getCell(37).getNumericCellValue());
            salary.setBeforeTax(row.getCell(38).getNumericCellValue());
            salary.setTax(row.getCell(39).getNumericCellValue());
            salary.setAfterTax(row.getCell(40).getNumericCellValue());
            salary.setRemark(getStringCellValue(row.getCell(41)));
            salary.setEmail(getStringCellValue(row.getCell(42)));

            salaries.add(salary);
        }
        is.close();
        return salaries;
    }

    /**
     * 设置合并单元格的边框
     * @param style 要设置的边框的样式
     * @param cellAddresses 要设置的合并的单元格
     * @param sheet 要设置的合并的单元格所在的表单
     */
    private static void setRegionBorderStyle(BorderStyle style, CellRangeAddress cellAddresses, Sheet sheet) {
        RegionUtil.setBorderTop(style, cellAddresses, sheet);
        RegionUtil.setBorderBottom(style, cellAddresses, sheet);
        RegionUtil.setBorderLeft(style, cellAddresses, sheet);
        RegionUtil.setBorderRight(style, cellAddresses, sheet);
    }

    /**
     * 设置普通单元格的边框
     * @param style 要设置的边框的样式
     * @param cellStyle 单元格样式对象
     */
    private static void setCellBorderStyle(BorderStyle style, CellStyle cellStyle) {
        cellStyle.setBorderTop(style);
        cellStyle.setBorderBottom(style);
        cellStyle.setBorderLeft(style);
        cellStyle.setBorderRight(style);
    }

    /**
     * 设置标题单元格样式
     * @param workbook 工作薄对象
     * @return 单元格样式对象
     */
    private static CellStyle getTitleCellStyle(Workbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();

        //设置字体
        Font font = workbook.createFont();
        font.setFontName("黑体");
        font.setFontHeightInPoints((short) 24);
        font.setColor((short) 10);
        cellStyle.setFont(font);

        //设置文字居中显示
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        return cellStyle;
    }


    /**
     * 设置表头单元格样式
     * @param workbook 工作薄对象
     * @return 单元格样式对象
     */
    private static CellStyle getHeaderCellStyle(Workbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();

        //设置字体
        Font font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 20);
        font.setBold(true);
        cellStyle.setFont(font);

        //设置文字居中显示
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        //设置单元格的边框
        setCellBorderStyle(BorderStyle.THIN, cellStyle);

        return cellStyle;
    }

    /**
     * 设置表体单元格样式
     * @param workbook 工作薄对象
     * @return 单元格样式对象
     */
    private static CellStyle getBodyCellStyle(Workbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();

        //设置字体
        Font font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 16);
        cellStyle.setFont(font);

        //设置文字居中显示
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        //设置单元格的边框
        setCellBorderStyle(BorderStyle.THIN, cellStyle);

        return cellStyle;
    }

    /**
     * 获取单元格的值的字符串
     * @param cell 单元格对象
     * @return cell单元格的值的字符串
     */
    private static String getStringCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        CellType cellType = cell.getCellType();
        switch (cellType) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                double value = cell.getNumericCellValue();
                return String.valueOf(Math.round(value));
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            default:
                return null;
        }
    }

    public static void main(String[] args) throws IOException {
        readExcel("/Users/jesse/Documents/壹伍壹拾成都分公司工资-202003.xlsx");
    }

    public static void readFromMultiFile(){

    }

}