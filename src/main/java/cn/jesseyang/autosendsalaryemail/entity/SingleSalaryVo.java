package cn.jesseyang.autosendsalaryemail.entity;

import lombok.Data;

@Data
public class SingleSalaryVo {
    String year;
    String month;
    Salary salary;
}
