package cn.jesseyang.autosendsalaryemail.entity;

import lombok.Data;

@Data
public class Salary {
    private String name;
    private String department;
    private double workday;
    private double actualWorkday;
    private double personalLeave;
    private double illnessLeave;
    private double annualLeave;
    private double adjustLeave;
    private double weddingLeave;
    private double funeralLeave;
    private double companyChildBirthLeave;
    private double childBirthCheckLeave;
    private double childBirthLeave;
    private double notWork;
    private double businessTrip;
    private double overwork;
    private double specialOverwork;
    private double beLateTimes;
    private double beLateMinutes;
    private double earlyTimes;
    private double earlyMinutes;

    private double standSalary;
    private double performanceSalary;
    private double extraSalary;
    private double highSalary;
    private double food;
    private double travel;
    private double phone;
    private double transportation;

    private double beLateReduce;
    private double personalLeaveReduce;
    private double salaryReduce;

    private double income;
    private double socialSecurity;
    private double accumulationFund;
    private double beforeTax;
    private double tax;
    private double afterTax;
    private String remark;

    private String email;

}
