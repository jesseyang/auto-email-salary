package cn.jesseyang.autosendsalaryemail.entity;

import lombok.Data;

import java.util.List;

@Data
public class SalaryVo {
    List<Salary> salaryList;
    String month;
    String year;
}
